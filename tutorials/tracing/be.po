msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2020-09-04 19:55-0400\n"
"PO-Revision-Date: \n"
"Last-Translator: Hleb Valoshka <375gnu@gmail.com>\n"
"Language-Team: belarusian <i18n@mova.org>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Belarusian\n"
"X-Poedit-Country: BELARUS\n"
"X-Poedit-SourceCharset: utf-8\n"

#: tracing-f06.svg:49(format) tracing-f05.svg:49(format)
#: tracing-f04.svg:49(format) tracing-f03.svg:49(format)
#: tracing-f02.svg:49(format) tracing-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tracing-f06.svg:70(tspan) tracing-f05.svg:70(tspan)
#: tracing-f04.svg:70(tspan) tracing-f03.svg:70(tspan)
#: tracing-f02.svg:70(tspan)
#, no-wrap
msgid "Original Image"
msgstr "Спачатны відарыс"

#: tracing-f06.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Абведзены відарыс / Выніковы шлях — спрошчаны"

#: tracing-f06.svg:86(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 вузлы)"

#: tracing-f05.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Абведзены відарыс / Выніковы шлях"

#: tracing-f05.svg:86(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1.551 вузел)"

#: tracing-f04.svg:81(tspan) tracing-f04.svg:97(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Агрублены (12 колераў)"

#: tracing-f04.svg:86(tspan) tracing-f03.svg:86(tspan)
#: tracing-f02.svg:86(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr "Нутро, бяз контура"

#: tracing-f04.svg:102(tspan) tracing-f03.svg:102(tspan)
#: tracing-f02.svg:102(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr "Контур, без нутра"

#: tracing-f03.svg:81(tspan) tracing-f03.svg:97(tspan)
#, no-wrap
msgid "Edge Detected"
msgstr "Край вызначаны"

#: tracing-f02.svg:81(tspan) tracing-f02.svg:97(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr "Адсячэньне яркасьці"

#: tracing-f01.svg:70(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "Галоўныя выборы дыялёґу абводжаньня"

#: tutorial-tracing.xml:7(title)
#, fuzzy
msgid "Tracing bitmaps"
msgstr "Абводжаньне"

#: tutorial-tracing.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-tracing.xml:13(para)
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Адна з асаблівасьцяў Inkscape — гэта інструмэнт абводжаньня растравых "
"відарысаў у элемэнт рысунку SVG &lt;path&gt; (шлях). Гэтыя сьціслыя зацемкі "
"мусяць дапамагчы вам пазнаёміцца як ён працуе."

#: tutorial-tracing.xml:20(para)
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"Сёньня Inkscape карыстаецца рухавіком абводжаньня Potrace (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>), "
"напісаным Peter Selinger. У будучыні мы, напэўна, дазволім іншыя праґрамы "
"абводжаньня, сёньня ж, аднак, гэты выдатны інструмэнт цалкам задавальняе "
"нашыя патрэбы."

#: tutorial-tracing.xml:27(para)
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Майце на ўвазе, што прызначэньне абводжаньніка ня ў тым, каб ствараць "
"дакладную копію спачатнага відарыса, і ня ў тым, каб ствараць канчатковы "
"прадукт. Ніякі аўтаабводжаньнік ня можа гэтага зрабіць. Што ён можа — "
"стварыць набор крывых, якія можна выкарыстоўваць як рэсурс для рысунку."

#: tutorial-tracing.xml:34(para)
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"Potrace апрацоўвае чорна-белы растар і стварае набор крывых. Для Potrace мы "
"сёньня маем тры віды ўваходных фільтраў ператварэньня з сырога відарыса ў "
"нешта, што Potrace можа выкарыстоўваць."

#: tutorial-tracing.xml:40(para)
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Звычайна, чым цямнейшыя піксэлі ў прамежкавым растры, тым больш абводжаньняў "
"Potrace будзе рабіць. Чым больш абводжаньняў, тым больш часу працэсара "
"патрэбна, а элемэнт &lt;path&gt; становіцца ўсё большым. Пажадана, каб "
"карыстальнік паэкспэрымэнтаваў спачатку з больш сьветлым прамежкавым "
"відарысам, паступова зацямняючы яго, каб атрымаць пажаданыя прапорцыі й "
"складанасьць выніковага шляха."

#: tutorial-tracing.xml:48(para)
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"Каб выкарыстоўваць абводжаньнік загрузіце ці імпартуйце відарыс, вылучыце "
"яго й выберыце ў мэню <command>Шлях &gt; Абвесьці растар</command>  ці "
"націсьніце <keycap>Shift+Alt+B</keycap>."

#: tutorial-tracing.xml:60(para)
msgid "The user will see the three filter options available:"
msgstr "Карыстальнік пабачыць тры выборы фільтра:"

#: tutorial-tracing.xml:65(para)
#, fuzzy
msgid "Brightness Cutoff"
msgstr "Адсячэньне яркасьці"

#: tutorial-tracing.xml:70(para)
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Тут проста выкарыстоўваецца сума чырвонага, зялёнага й сіняга (ці адценьняў "
"шэрага) складнікаў піксэля каб вызначыць, мусіць ён быць чорным ці белым. "
"Парог можна задаваць ад 0,0 (чорны) да 1,0 (белы). Чым большы парог, тым "
"меншая колькасьць піксэляў будзе залічаная ў «белыя», і прамежкавы выдарыс "
"стане цямнейшым."

#: tutorial-tracing.xml:86(para)
#, fuzzy
msgid "Edge Detection"
msgstr "Край вызначаны"

#: tutorial-tracing.xml:91(para)
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"Тут выкарыстоўваецца альґарытм вызначэньня краю, прыдуманы J. Canny як "
"спосаб хуткага пошуку ізакліналяў падобнага кантрасту. Тут ствараецца "
"прамежкавы відарыс, які ня так падобны на спачатны відарыс, як вынік "
"Адсячэньня яркасьці, але, найхутчэй, выдасьць інфармацыю аб крывых, якая ў "
"іншым выпадку будзе праіґнараваная. Настройка парогу (0,0 — 1,0) рэґулюе ці "
"будзе піксэль, сумежны да краю кантрасту, уключаны ў вынік. Гэтая настройка "
"можа рэґуляваць цёмнасьць ці таўшчыню краю ў выніку."

#: tutorial-tracing.xml:109(para)
msgid "Color Quantization"
msgstr "Агрубленьне колераў"

#: tutorial-tracing.xml:114(para)
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Вынік фільтру стварае прамежкавы відарыс, які вельмі адрозьніваецца ад "
"першых двух, але, ўсё адно, вельмі карысны. Замест паказу ізакліналяў "
"якрасьці ці кантрасту, тут шукаюцца краі, дзе мяняюцца колеры, нават пры "
"аднолькавых яркасьці й кантрасьце. Настройка, Колькасьць колераў, вызначае "
"колькі выніковых колераў будзе, калі прамежкавы відарыс — каляровы. Потым "
"вызначаецца чорны/белы на падставе таго, цотны ці няцотны індэкс мае колер."

#: tutorial-tracing.xml:130(para)
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Карыстальнік мусіць паспрабаваць усе тры фільтры, і паназіраць за рознымі "
"вынікамі для розных відаў уваходных відарысаў. Заўжды будзе відарыс, зь "
"якім  нейкі фільтар працуе лепш за іншыя."

#: tutorial-tracing.xml:136(para)
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Пасьля абводжаньня, пажадана скарыстаць <command>Шлях &gt; Спросьціць</"
"command> (<keycap>Ctrl+L</keycap>) на выніковым шляху, каб зьменшыць "
"колькасьць вузлоў. Гэта зробіць вынік Potrace больш простым для праўкі. "
"Напрыклад, вось тыповыя абводжаньні Старога чалавека, які іграе на ґітары:"

#: tutorial-tracing.xml:150(para)
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Зьвярніце ўвагу на аграмадную колькасьць вузлоў на шляху. Пасьля націсканьня "
"<keycap>Ctrl+L</keycap>, атрымоўваеем тыповы вынік:"

#: tutorial-tracing.xml:162(para)
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"Выява цяпер больш прыблізная й грубая, але рысунак больш просты й яго "
"прасьцей правіць. Майце на ўвазе, што вам патрэбная не дакладная пабудова "
"відарыса, але набор крывых, якія можна выкарыстоўваць у сваім рысунку."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr "Глеб Валошка  <375gnu@gmail.com>, 2010"

#~ msgid "Optimal Edge Detection"
#~ msgstr "Аптымальнае вызначэньне краю"
